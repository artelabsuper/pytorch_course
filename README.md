# Pytorch Course

This project contains the jupyter slides of the Uninsubria pytorch seminar titled "Introducing Deep Learning with PyTorch".

<img src="Introducing Deep Learning with PyTorch.png">

## Usage with venv
Requirements:
* python3.10
* venv

Run the following commands
```bash
python3.10 -m venv venv # onli de first time

source venv/bin/activate
# on windiws: source venv/bin/activate.bat
# on fish shell: source venv/bin/activate.fish
 
pip install -r requirements.txt

jupyter notebook

# go to browser ate localhost:8888/?token=... like suggested at the end of the command
# elable View>Cell Toolbar>Slideshow
``` 

## Usage with docker

```bash
docker-compose up
```

## Bibliography
* python
* venv
* jupyter
* rise
* pytorch
