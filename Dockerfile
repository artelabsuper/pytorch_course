FROM python:3.10.6-buster

RUN pip install --upgrade pip && pip install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cpu

WORKDIR /work
ADD requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt

CMD jupyter notebook --allow-root --ip "0.0.0.0"