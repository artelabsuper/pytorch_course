# Pytorch Seminar

This project contains the jupyter slides of the Uninsubria pytorch seminar titled "Introducing Deep Learning with PyTorch".

For the notebook about diffusion models, presented by Digitiamo go to this link https://github.com/banda-larga/diffusion-lecture-it

