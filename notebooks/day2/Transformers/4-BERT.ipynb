{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c75d4782",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "sources: (1) [how-to-code-bert-using-pytorch](https://neptune.ai/blog/how-to-code-bert-using-pytorch-tutorial), (2) [bert-explained-state-of-the-art-language-model-for-nlp](https://towardsdatascience.com/bert-explained-state-of-the-art-language-model-for-nlp-f8b21a9b6270)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b6281d90",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# BERT: Bidirectional Encoder Representation with Transformers\n",
    "\n",
    "* A language model introduced in 2018, by Google with the paper titled “[Pre-training of deep bidirectional transformers for language understanding](https://arxiv.org/pdf/1810.04805.pdf)”. \n",
    "* BERT achieved **state-of-the-art** performance in tasks like `Question-Answering`, `Natural Language Inference`, `Classification`, and General language understanding evaluation or (GLUE).\n",
    "* BERT release was followed after the release of three architectures that also achieved state-of-the-art performances. These models were: \n",
    "  *  ULM-Fit (January 2018),  ELMo (February 2018),   OpenAI **GPT** (June 2018) \n",
    "* In the original BERT paper, it was compared with GPT on the [General Language understanding evaluation benchmark](https://gluebenchmark.com/), and here are the results. \n",
    "\n",
    "<img src=\"images/fig4-BERT-GPT-comparison.webp\" width=\"700\">"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c45db58b",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## What is BERT?\n",
    "\n",
    "* **BERT** stands for “`Bidirectional Encoder Representation with Transformers`”. \n",
    "* BERT extracts patterns from the data by passing it through an **encoder**. \n",
    "* Since BERT’s goal is to generate a language model, **only the encoder mechanism is necessary**.\n",
    "* BERT is based on stacked layers of encoders, stacked on top of each other.\n",
    "<img src=\"images/fig4-BERT-architecture.jpeg\" width=\"600\">\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b5eeadbc",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### BERT is bidirectional\n",
    "\n",
    "* As opposed to directional models, which read the text input sequentially (left-to-right or right-to-left), the **Transformer encoder reads the entire sequence of words at once**. \n",
    "* Therefore it is considered **bidirectional**, though it would be `more accurate` to say that it’s non-directional. \n",
    "* This characteristic allows the model to **learn the context** of a word based on all of its surroundings (left and right of the word).\n",
    "\n",
    "<img src=\"images/fig4-BERT-bidirectional.png\" width=\"400\">"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eab0ab23",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## BERT training paradigms\n",
    "### Pre-training\n",
    "* the model is trained on a **large dataset** to extract patterns. \n",
    "* This is generally an **unsupervised** learning task where the model is trained on an unlabelled dataset like the data from a big corpus like Wikipedia.  \n",
    "* More specifically, we can say that BERT falls into a **self-supervised** model:\n",
    "  * it can generate inputs and labels from the raw corpus without being explicitly programmed by humans. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78406d93",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "### Fine-tuning\n",
    "\n",
    "* the model is trained for downstream tasks like `Classification`, `Text-Generation`, `Language Translation`, `Question-Answering`, and so forth. \n",
    "* Essentially, you can download a pre-trained model and then **Transfer-learn** the model on your data. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25d142fc",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Pre-training strategies\n",
    "\n",
    "### 1 - Masked Language Model (MLM)\n",
    "\n",
    "* Before feeding word sequences into BERT, 15% of the words in each sequence are replaced with a **[MASK] token**. \n",
    "* The model then attempts to **predict** the original value of the masked words, **based on the context** provided by the other, non-masked, words in the sequence.\n",
    "\n",
    "<img src=\"images/fig4-BERT-masked-training.png\" width=\"600\">"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e528116e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 2 - Next Sentence Prediction (NSP)\n",
    "\n",
    "* the model receives **pairs of sentences** as input and learns to `predict if the second sentence in the pair is the subsequent sentence in the original document`. \n",
    "* During training, **50% of the inputs** are a pair in which the second sentence is the subsequent sentence in the original document, while in the **other 50%** a **random** sentence from the corpus is chosen as the second sentence. \n",
    "* The **assumption** is that the random sentence will be disconnected from the first sentence."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ef4f6e6",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 2 - Next Sentence Prediction (NSP)\n",
    "\n",
    "To help the model **distinguish between the two sentences in training**, the input is processed in the following way before entering the model:\n",
    "\n",
    "1.    A **[CLS] token** is inserted at the beginning of the first sentence and a **[SEP] token** is inserted at the end of each sentence.\n",
    "2.    A **sentence embedding** indicating Sentence A or Sentence B is added to each token. Sentence embeddings are similar in concept to token embeddings with a vocabulary of 2.\n",
    "3.    A **positional embedding** is added to each token to indicate its position in the sequence. \n",
    "    \n",
    "<img src=\"images/fig4-BERT-mix-sentences.png\" width=\"700\">"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0284093c",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Pre-training example\n",
    "\n",
    "* You can pretrained BERT on **MLM + NSP** at the same time. Or you can only use one of the two strategies.\n",
    "* if sentence **A** is  `“[CLS] the man [MASK] to the store”` and sentence **B** is `“penguin [MASK] are flightless birds [SEP]”`, then BERT will be able to classify whether both the **sentences are continuous** or not. \n",
    "\n",
    "\n",
    "<img src=\"images/fig4-BERT-pretraining-example.webp\" width=\"700\">"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3466ee50",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Fine tuning"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0acb0b18",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Learn how to code BERT using PyTorch\n",
    "\n",
    "We will break the entire program into 4 sections:\n",
    "\n",
    "*    Preprocessing\n",
    "*    Building model\n",
    "*    Loss and Optimization\n",
    "*    Training"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "86320d74",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "### Import libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "9e39dfe8",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "import math\n",
    "import re\n",
    "from random import *\n",
    "import numpy as np\n",
    "import torch\n",
    "import torch.nn as nn\n",
    "import torch.optim as optim"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94f07bd0",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Preprocessing\n",
    "\n",
    "We start by assigning a **raw text for training**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "eb253744",
   "metadata": {},
   "outputs": [],
   "source": [
    "text = (\n",
    "       'Hello, how are you? I am Romeo.\\n'\n",
    "       'Hello, Romeo My name is Juliet. Nice to meet you.\\n'\n",
    "       'Nice meet you too. How are you today?\\n'\n",
    "       'Great. My baseball team won the competition.\\n'\n",
    "       'Oh Congratulations, Juliet\\n'\n",
    "       'Thanks you Romeo'\n",
    "   )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "500fed8e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Then we will clean the data by:\n",
    "\n",
    "*    Making the sentences into lower case.\n",
    "*    Creating vocabulary"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "18c1ee2a",
   "metadata": {},
   "outputs": [],
   "source": [
    "sentences = re.sub(\"[.,!?\\\\-]\", '', text.lower()).split('\\n')  # filter '.', ',', '?', '!'\n",
    "word_list = list(set(\" \".join(sentences).split())) # the vocabulary"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "c8867482",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['hello how are you i am romeo', 'hello romeo my name is juliet nice to meet you', 'nice meet you too how are you today', 'great my baseball team won the competition', 'oh congratulations juliet', 'thanks you romeo']\n"
     ]
    }
   ],
   "source": [
    "print(sentences)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "d67fb614",
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['meet', 'too', 'are', 'great', 'oh', 'baseball', 'today', 'my', 'you', 'won', 'is', 'how', 'am', 'to', 'thanks', 'competition', 'the', 'name', 'nice', 'hello', 'romeo', 'i', 'juliet', 'team', 'congratulations']\n"
     ]
    }
   ],
   "source": [
    "print(word_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "673cf1e6",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "BERT takes **special tokens** during training. We need to add the following tokens to the vocabulary\n",
    "\n",
    "| Token | purpose |\n",
    "|-------|---------|\n",
    "| [CLS] |The first token is always classification |\n",
    "| [SEP] |Separates two sentences |\n",
    "| [END] | End the sentence |\n",
    "| [PAD] | Use to truncate the sentence with equal length |\n",
    "| [MASK]| Use to create a mask by replacing the original word |\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "3721ba98",
   "metadata": {},
   "outputs": [],
   "source": [
    "word_dict = {'[PAD]': 0, '[CLS]': 1, '[SEP]': 2, '[MASK]': 3}\n",
    "for i, w in enumerate(word_list):\n",
    "    word_dict[w] = i + 4\n",
    "number_dict = {i: w for i, w in enumerate(word_dict)}\n",
    "vocab_size = len(word_dict)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "6fc90368",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'[PAD]': 0, '[CLS]': 1, '[SEP]': 2, '[MASK]': 3, 'meet': 4, 'too': 5, 'are': 6, 'great': 7, 'oh': 8, 'baseball': 9, 'today': 10, 'my': 11, 'you': 12, 'won': 13, 'is': 14, 'how': 15, 'am': 16, 'to': 17, 'thanks': 18, 'competition': 19, 'the': 20, 'name': 21, 'nice': 22, 'hello': 23, 'romeo': 24, 'i': 25, 'juliet': 26, 'team': 27, 'congratulations': 28}\n",
      "{0: '[PAD]', 1: '[CLS]', 2: '[SEP]', 3: '[MASK]', 4: 'meet', 5: 'too', 6: 'are', 7: 'great', 8: 'oh', 9: 'baseball', 10: 'today', 11: 'my', 12: 'you', 13: 'won', 14: 'is', 15: 'how', 16: 'am', 17: 'to', 18: 'thanks', 19: 'competition', 20: 'the', 21: 'name', 22: 'nice', 23: 'hello', 24: 'romeo', 25: 'i', 26: 'juliet', 27: 'team', 28: 'congratulations'}\n"
     ]
    }
   ],
   "source": [
    "print(word_dict)\n",
    "print(number_dict)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec4d9603",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### From words to indexes\n",
    "\n",
    "we convert everything to an **index** from the word dictionary. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "375d96eb",
   "metadata": {},
   "outputs": [],
   "source": [
    "token_list = list()\n",
    "for sentence in sentences:\n",
    "    arr = [word_dict[s] for s in sentence.split()]\n",
    "    token_list.append(arr)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "7b8c51cc",
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[23, 15, 6, 12, 25, 16, 24], [23, 24, 11, 21, 14, 26, 22, 17, 4, 12], [22, 4, 12, 5, 15, 6, 12, 10], [7, 11, 9, 27, 13, 20, 19], [8, 28, 26], [18, 12, 24]]\n"
     ]
    }
   ],
   "source": [
    "print(token_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab9106a8",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Prepare for token embeddings"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "9c50d6f8",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "sentence A: 'hello how are you i am romeo' -> [23, 15, 6, 12, 25, 16, 24]\n",
      "sentence B: 'hello romeo my name is juliet nice to meet you' -> [23, 24, 11, 21, 14, 26, 22, 17, 4, 12]\n",
      "sentence A+B: [1, 23, 15, 6, 12, 25, 16, 24, 2, 23, 24, 11, 21, 14, 26, 22, 17, 4, 12, 2]\n"
     ]
    }
   ],
   "source": [
    "tokens_a_index, tokens_b_index= randrange(len(sentences)), randrange(len(sentences))\n",
    "tokens_a, tokens_b= token_list[tokens_a_index], token_list[tokens_b_index]\n",
    "print(f\"sentence A: '{sentences[tokens_a_index]}' -> {tokens_a}\")\n",
    "print(f\"sentence B: '{sentences[tokens_b_index]}' -> {tokens_b}\")\n",
    "input_ids = [word_dict['[CLS]']] + tokens_a + [word_dict['[SEP]']] + tokens_b + [word_dict['[SEP]']]\n",
    "print(f\"sentence A+B: {input_ids}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "006565c8",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Prepare for sentence embeddings"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "d2959d90",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "sentence A+B: [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]\n"
     ]
    }
   ],
   "source": [
    "segment_ids = [0] * (1 + len(tokens_a) + 1) + [1] * (len(tokens_b) + 1)\n",
    "print(f\"sentence A+B: {segment_ids}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3c51bd56",
   "metadata": {},
   "source": [
    "**Config**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "c6f8b4dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "maxlen = 30 # maximum of length\n",
    "batch_size = 6\n",
    "max_pred = 5  # max tokens to be predicted (masked)\n",
    "n_layers = 6 # number of Encoder of Encoder Layer\n",
    "n_heads = 12 # number of heads in Multi-Head Attention\n",
    "d_model = 768 # Embedding Size\n",
    "d_ff = 768 * 4  # 4*d_model, FeedForward dimension\n",
    "d_k = d_v = 64  # dimension of K(=Q), V\n",
    "n_segments = 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9102ea96",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### ground-truth for MLM strategy\n",
    "GT = `masked_pos` and `masked_tokens`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "a62b4588",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tokens to be predicted (masked): 3\n",
      "candidates for masked pos: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]\n",
      "candidates for masked pos: [10, 11, 12, 2, 14, 15, 13, 9, 18, 4, 3, 5, 16, 1, 7, 17, 6]\n",
      "masked_pos: [10, 11, 12]\n",
      "masked_tokens: '[24, 11, 21]' -> ['romeo', 'my', 'name'] --> ['[CLS]', 'hello', 'how', 'are', 'you', 'i', 'am', 'romeo', '[SEP]', 'hello', '[MASK]', 'baseball', '[MASK]', 'is', 'juliet', 'nice', 'to', 'meet', 'you', '[SEP]']\n"
     ]
    }
   ],
   "source": [
    "max_pred = 5  # max tokens to be predicted (masked)\n",
    "#MASK LM\n",
    "n_pred =  min(max_pred, max(1, int(round(len(input_ids) * 0.15)))) # 15 % of tokens in one sentence\n",
    "print(\"tokens to be predicted (masked):\", n_pred)\n",
    "cand_maked_pos = [i for i, token in enumerate(input_ids)\n",
    "                  if token != word_dict['[CLS]'] and token != word_dict['[SEP]']]\n",
    "print(\"candidates for masked pos:\", cand_maked_pos)\n",
    "shuffle(cand_maked_pos)\n",
    "print(\"candidates for masked pos:\", cand_maked_pos)\n",
    "masked_tokens, masked_pos = [], []\n",
    "for pos in cand_maked_pos[:n_pred]:\n",
    "    masked_pos.append(pos)\n",
    "    masked_tokens.append(input_ids[pos])\n",
    "    if random() < 0.8:  # 80%\n",
    "        input_ids[pos] = word_dict['[MASK]'] # make mask\n",
    "    elif random() < 0.5:  # 10%\n",
    "        index = randint(0, vocab_size - 1) # random index in vocabulary\n",
    "        input_ids[pos] = word_dict[number_dict[index]] # replace\n",
    "print(\"masked_pos:\", masked_pos)\n",
    "print(f\"masked_tokens: '{masked_tokens}' -> {[number_dict[t] for t in masked_tokens]} --> {[number_dict[i] for i in input_ids]}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ecd3b320",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### ground-truth for NSP strategy\n",
    "* we have to create a label that predicts whether the sentence has a consecutive sentence or not, i.e. **IsNext** or **NotNext**. \n",
    "* So we assign True for every sentence that precedes the next sentence and we use a conditional statement to do that. \n",
    "<img src=\"images/fig4-bert-nsp.png\" width=\"700\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "5692dce4",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "def make_batch():\n",
    "    batch = []\n",
    "    positive = negative = 0\n",
    "    while positive != batch_size/2 or negative != batch_size/2:\n",
    "        tokens_a_index, tokens_b_index= randrange(len(sentences)), randrange(len(sentences))\n",
    "        tokens_a, tokens_b= token_list[tokens_a_index], token_list[tokens_b_index]\n",
    "\n",
    "        input_ids = [word_dict['[CLS]']] + tokens_a + [word_dict['[SEP]']] + tokens_b + [word_dict['[SEP]']]\n",
    "\n",
    "        segment_ids = [0] * (1 + len(tokens_a) + 1) + [1] * (len(tokens_b) + 1)\n",
    "\n",
    "        #MASK LM\n",
    "        n_pred =  min(max_pred, max(1, int(round(len(input_ids) * 0.15)))) # 15 % of tokens in one sentence\n",
    "\n",
    "        cand_maked_pos = [i for i, token in enumerate(input_ids)\n",
    "                          if token != word_dict['[CLS]'] and token != word_dict['[SEP]']]\n",
    "        shuffle(cand_maked_pos)\n",
    "        masked_tokens, masked_pos = [], []\n",
    "        for pos in cand_maked_pos[:n_pred]:\n",
    "            masked_pos.append(pos)\n",
    "            masked_tokens.append(input_ids[pos])\n",
    "            if random() < 0.8:  # 80%\n",
    "                input_ids[pos] = word_dict['[MASK]'] # make mask\n",
    "            elif random() < 0.5:  # 10%\n",
    "                index = randint(0, vocab_size - 1) # random index in vocabulary\n",
    "                input_ids[pos] = word_dict[number_dict[index]] # replace\n",
    "\n",
    "        # Zero Paddings\n",
    "        n_pad = maxlen - len(input_ids)\n",
    "        input_ids.extend([0] * n_pad)\n",
    "        segment_ids.extend([0] * n_pad)\n",
    "\n",
    "        # Zero Padding (100% - 15%) tokens\n",
    "        if max_pred > n_pred:\n",
    "            n_pad = max_pred - n_pred\n",
    "            masked_tokens.extend([0] * n_pad)\n",
    "            masked_pos.extend([0] * n_pad)\n",
    "\n",
    "        if tokens_a_index + 1 == tokens_b_index and positive < batch_size/2:\n",
    "            batch.append([input_ids, segment_ids, masked_tokens, masked_pos, True]) # IsNext\n",
    "            positive += 1\n",
    "        elif tokens_a_index + 1 != tokens_b_index and negative < batch_size/2:\n",
    "            batch.append([input_ids, segment_ids, masked_tokens, masked_pos, False]) # NotNext\n",
    "            negative += 1\n",
    "    return batch"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "4ba6a1a4",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "batch = make_batch()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "id": "a642a8a2",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1, 3, 24, 11, 21, 14, 26, 22, 17, 4, 12, 2, 3, 11, 9, 27, 13, 3, 19, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [20, 7, 23, 0, 0], [17, 12, 1, 0, 0], False]\n",
      "[[1, 7, 11, 9, 27, 13, 20, 3, 2, 18, 12, 24, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [19, 20, 0, 0, 0], [7, 6, 0, 0, 0], False]\n",
      "[[1, 18, 3, 24, 2, 23, 15, 6, 12, 25, 16, 24, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [18, 12, 0, 0, 0], [1, 2, 0, 0, 0], False]\n",
      "[[1, 23, 15, 6, 12, 25, 16, 24, 2, 3, 24, 11, 21, 14, 3, 22, 3, 4, 12, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [23, 26, 17, 0, 0], [9, 14, 16, 0, 0], True]\n",
      "[[1, 8, 28, 26, 2, 3, 12, 24, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [18, 0, 0, 0, 0], [5, 0, 0, 0, 0], True]\n",
      "[[1, 3, 28, 26, 2, 18, 12, 24, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [8, 0, 0, 0, 0], [1, 0, 0, 0, 0], True]\n"
     ]
    }
   ],
   "source": [
    "for p in batch:\n",
    "    print(p)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "id": "1814855e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(False, False, False, True, True, True)"
      ]
     },
     "execution_count": 55,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# select the expected outputs from a batch when using a NSP strategy\n",
    "input_ids, segment_ids, masked_tokens, masked_pos, isNext = zip(*batch)\n",
    "isNext"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dac23822",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Embeddings\n",
    "We need to create a function that formats the input sequences for three types of **embeddings**: `token embedding`, `segment embedding`, and `position embedding`.\n",
    "<img src=\"images/fig4-BERT-mix-sentences.png\" width=\"700\">"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b41b7791",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Embedding layers\n",
    "* The embedding is the first layer in BERT that takes the input and creates a lookup table. \n",
    "* The parameters of the embedding layers are learnable, which means when the learning process is over the embeddings will cluster similar words together. \n",
    "* BERT creates three embeddings for \n",
    "  *  Token \n",
    "  *  Segments\n",
    "  *  Position. \n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "83c32534",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "class Embedding(nn.Module):\n",
    "   def __init__(self):\n",
    "       super(Embedding, self).__init__()\n",
    "       self.tok_embed = nn.Embedding(vocab_size, d_model)  # token embedding\n",
    "       self.pos_embed = nn.Embedding(maxlen, d_model)  # position embedding\n",
    "       self.seg_embed = nn.Embedding(n_segments, d_model)  # segment(token type) embedding\n",
    "       self.norm = nn.LayerNorm(d_model)\n",
    "\n",
    "   def forward(self, x, seg):\n",
    "       seq_len = x.size(1)\n",
    "       pos = torch.arange(seq_len, dtype=torch.long)\n",
    "       pos = pos.unsqueeze(0).expand_as(x)  # (seq_len,) -> (batch_size, seq_len)\n",
    "       embedding = self.tok_embed(x) + self.pos_embed(pos) + self.seg_embed(seg)\n",
    "       return self.norm(embedding)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 60,
   "id": "6774e705",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tensor([[ 1,  7, 11,  9, 27, 13, 20,  3,  2, 18, 12, 24,  2,  0,  0,  0,  0,  0,\n",
      "          0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0]])\n",
      "tensor([0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,\n",
      "        0, 0, 0, 0, 0, 0])\n",
      "torch.Size([1, 30, 768])\n"
     ]
    }
   ],
   "source": [
    "# example that computes the three embeddings\n",
    "emb = Embedding()\n",
    "# x = torch.randint(high=len(word_dict),size=(1,6)) # input sentence with 6 random ids\n",
    "# x_seg = torch.tensor([1,1,1,0,0,0])\n",
    "# exercise: print the x tensor as a sequence of words from our dictionary\n",
    "x = torch.tensor([input_ids[1]])\n",
    "print(x)\n",
    "x_seg = torch.tensor(segment_ids[1])\n",
    "print(x_seg)\n",
    "y = emb(x, x_seg) # output embedding\n",
    "print(y.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8dd3d6f",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Transformer layer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "id": "a5fb433d",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "class MultiHeadAttention(nn.Module):\n",
    "    def __init__(self):\n",
    "        super(MultiHeadAttention, self).__init__()\n",
    "        self.W_Q = nn.Linear(d_model, d_k * n_heads)\n",
    "        self.W_K = nn.Linear(d_model, d_k * n_heads)\n",
    "        self.W_V = nn.Linear(d_model, d_v * n_heads)\n",
    "    def forward(self, Q, K, V, attn_mask):\n",
    "        # q: [batch_size x len_q x d_model], k: [batch_size x len_k x d_model], v: [batch_size x len_k x d_model]\n",
    "        residual, batch_size = Q, Q.size(0)\n",
    "        # (B, S, D) -proj-> (B, S, D) -split-> (B, S, H, W) -trans-> (B, H, S, W)\n",
    "        q_s = self.W_Q(Q).view(batch_size, -1, n_heads, d_k).transpose(1,2)  # q_s: [batch_size x n_heads x len_q x d_k]\n",
    "        k_s = self.W_K(K).view(batch_size, -1, n_heads, d_k).transpose(1,2)  # k_s: [batch_size x n_heads x len_k x d_k]\n",
    "        v_s = self.W_V(V).view(batch_size, -1, n_heads, d_v).transpose(1,2)  # v_s: [batch_size x n_heads x len_k x d_v]\n",
    "\n",
    "        attn_mask = attn_mask.unsqueeze(1).repeat(1, n_heads, 1, 1) # attn_mask : [batch_size x n_heads x len_q x len_k]\n",
    "\n",
    "        # context: [batch_size x n_heads x len_q x d_v], attn: [batch_size x n_heads x len_q(=len_k) x len_k(=len_q)]\n",
    "        context, attn = ScaledDotProductAttention()(q_s, k_s, v_s, attn_mask)\n",
    "        context = context.transpose(1, 2).contiguous().view(batch_size, -1, n_heads * d_v) # context: [batch_size x len_q x n_heads * d_v]\n",
    "        output = nn.Linear(n_heads * d_v, d_model)(context)\n",
    "        return nn.LayerNorm(d_model)(output + residual), attn # output: [batch_size x len_q x d_model]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 66,
   "id": "858057c8",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "class PoswiseFeedForwardNet(nn.Module):\n",
    "    def __init__(self):\n",
    "        super(PoswiseFeedForwardNet, self).__init__()\n",
    "        self.fc1 = nn.Linear(d_model, d_ff)\n",
    "        self.fc2 = nn.Linear(d_ff, d_model)\n",
    "\n",
    "    def forward(self, x):\n",
    "        # (batch_size, len_seq, d_model) -> (batch_size, len_seq, d_ff) -> (batch_size, len_seq, d_model)\n",
    "        return self.fc2(gelu(self.fc1(x)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "id": "f260f8e6",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "class EncoderLayer(nn.Module):\n",
    "    def __init__(self):\n",
    "        super(EncoderLayer, self).__init__()\n",
    "        self.enc_self_attn = MultiHeadAttention()\n",
    "        self.pos_ffn = PoswiseFeedForwardNet()\n",
    "\n",
    "    def forward(self, enc_inputs, enc_self_attn_mask):\n",
    "        enc_outputs, attn = self.enc_self_attn(enc_inputs, enc_inputs, enc_inputs, enc_self_attn_mask) # enc_inputs to same Q,K,V\n",
    "        enc_outputs = self.pos_ffn(enc_outputs) # enc_outputs: [batch_size x len_q x d_model]\n",
    "        return enc_outputs, attn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 73,
   "id": "7138b5fb",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "class ScaledDotProductAttention(nn.Module):\n",
    "    def __init__(self):\n",
    "        super(ScaledDotProductAttention, self).__init__()\n",
    "\n",
    "    def forward(self, Q, K, V, attn_mask):\n",
    "        scores = torch.matmul(Q, K.transpose(-1, -2)) / np.sqrt(d_k) # scores : [batch_size x n_heads x len_q(=len_k) x len_k(=len_q)]\n",
    "        scores.masked_fill_(attn_mask, -1e9) # Fills elements of self tensor with value where mask is one.\n",
    "        attn = nn.Softmax(dim=-1)(scores)\n",
    "        context = torch.matmul(attn, V)\n",
    "        return context, attn "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 68,
   "id": "732eccd0",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "def gelu(x):\n",
    "    return x * 0.5 * (1.0 + torch.erf(x / math.sqrt(2.0)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 71,
   "id": "b14f5154",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "def get_attn_pad_mask(seq_q, seq_k):\n",
    "    batch_size, len_q = seq_q.size()\n",
    "    batch_size, len_k = seq_k.size()\n",
    "    # eq(zero) is PAD token\n",
    "    pad_attn_mask = seq_k.data.eq(0).unsqueeze(1)  # batch_size x 1 x len_k(=len_q), one is masking\n",
    "    return pad_attn_mask.expand(batch_size, len_q, len_k)  # batch_size x len_q x len_k"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "id": "151971e9",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "class BERT(nn.Module):\n",
    "   def __init__(self):\n",
    "       super(BERT, self).__init__()\n",
    "       self.embedding = Embedding()\n",
    "       self.layers = nn.ModuleList([EncoderLayer() for _ in range(n_layers)])\n",
    "       self.fc = nn.Linear(d_model, d_model)\n",
    "       self.activ1 = nn.Tanh()\n",
    "       self.linear = nn.Linear(d_model, d_model)\n",
    "       self.activ2 = gelu\n",
    "       self.norm = nn.LayerNorm(d_model)\n",
    "       self.classifier = nn.Linear(d_model, 2)\n",
    "       # decoder is shared with embedding layer\n",
    "       embed_weight = self.embedding.tok_embed.weight\n",
    "       n_vocab, n_dim = embed_weight.size()\n",
    "       self.decoder = nn.Linear(n_dim, n_vocab, bias=False)\n",
    "       self.decoder.weight = embed_weight\n",
    "       self.decoder_bias = nn.Parameter(torch.zeros(n_vocab))\n",
    "\n",
    "   def forward(self, input_ids, segment_ids, masked_pos):\n",
    "       output = self.embedding(input_ids, segment_ids)\n",
    "       enc_self_attn_mask = get_attn_pad_mask(input_ids, input_ids)\n",
    "       for layer in self.layers:\n",
    "           output, enc_self_attn = layer(output, enc_self_attn_mask)\n",
    "       # output : [batch_size, len, d_model], attn : [batch_size, n_heads, d_mode, d_model]\n",
    "       # it will be decided by first token(CLS)\n",
    "       h_pooled = self.activ1(self.fc(output[:, 0])) # [batch_size, d_model]\n",
    "       logits_clsf = self.classifier(h_pooled) # [batch_size, 2]\n",
    "\n",
    "       masked_pos = masked_pos[:, :, None].expand(-1, -1, output.size(-1)) # [batch_size, max_pred, d_model]\n",
    "\n",
    "       # get masked position from final output of transformer.\n",
    "       h_masked = torch.gather(output, 1, masked_pos) # masking position [batch_size, max_pred, d_model]\n",
    "       h_masked = self.norm(self.activ2(self.linear(h_masked)))\n",
    "       logits_lm = self.decoder(h_masked) + self.decoder_bias # [batch_size, max_pred, n_vocab]\n",
    "\n",
    "       return logits_lm, logits_clsf"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36e6e2ac",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Pre-Training BERT\n",
    "\n",
    "<img src=\"images/fig4-bert-pre-training.png\" width=\"700\">\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 75,
   "id": "e41b29a8",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "model = BERT()\n",
    "criterion = nn.CrossEntropyLoss()\n",
    "optimizer = optim.Adam(model.parameters(), lr=0.001)\n",
    "\n",
    "batch = make_batch()\n",
    "input_ids, segment_ids, masked_tokens, masked_pos, isNext = map(torch.LongTensor, zip(*batch))\n",
    "epoch = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 82,
   "id": "8a546352",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Epoch: 0030 cost = 3.286129\n"
     ]
    }
   ],
   "source": [
    "for epoch in range(epoch, epoch+10):\n",
    "    optimizer.zero_grad()\n",
    "    logits_lm, logits_clsf = model(input_ids, segment_ids, masked_pos)\n",
    "    loss_lm = criterion(logits_lm.transpose(1, 2), masked_tokens) # for masked LM\n",
    "    loss_lm = (loss_lm.float()).mean()\n",
    "    loss_clsf = criterion(logits_clsf, isNext) # for sentence classification\n",
    "    loss = loss_lm + loss_clsf\n",
    "    if (epoch + 1) % 10 == 0:\n",
    "        print('Epoch:', '%04d' % (epoch + 1), 'cost =', '{:.6f}'.format(loss))\n",
    "    loss.backward()\n",
    "    optimizer.step()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c71a0e5a",
   "metadata": {},
   "source": [
    "### Generalization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 83,
   "id": "af1bfe05",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello, how are you? I am Romeo.\n",
      "Hello, Romeo My name is Juliet. Nice to meet you.\n",
      "Nice meet you too. How are you today?\n",
      "Great. My baseball team won the competition.\n",
      "Oh Congratulations, Juliet\n",
      "Thanks you Romeo\n",
      "['[CLS]', 'am', 'meet', 'you', 'too', '[MASK]', 'are', 'you', 'today', '[SEP]', 'thanks', 'you', 'romeo', '[SEP]']\n",
      "masked tokens list :  [15, 22]\n",
      "predict masked tokens list :  [10, 10]\n",
      "isNext :  False\n",
      "predict isNext :  False\n"
     ]
    }
   ],
   "source": [
    "# Predict mask tokens ans isNext\n",
    "input_ids, segment_ids, masked_tokens, masked_pos, isNext = map(torch.LongTensor, zip(batch[0]))\n",
    "print(text)\n",
    "print([number_dict[w.item()] for w in input_ids[0] if number_dict[w.item()] != '[PAD]'])\n",
    "\n",
    "logits_lm, logits_clsf = model(input_ids, segment_ids, masked_pos)\n",
    "logits_lm = logits_lm.data.max(2)[1][0].data.numpy()\n",
    "print('masked tokens list : ',[pos.item() for pos in masked_tokens[0] if pos.item() != 0])\n",
    "print('predict masked tokens list : ',[pos for pos in logits_lm if pos != 0])\n",
    "\n",
    "logits_clsf = logits_clsf.data.max(1)[1].data.numpy()[0]\n",
    "print('isNext : ', True if isNext else False)\n",
    "print('predict isNext : ',True if logits_clsf else False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a15f4324",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
